from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.urls import reverse

# Create your models here.
#team hereda de models que es una clase  propia de Django
# El equipo tiene nombre, escudo, foto del equipo y la fecha en la que se lo creo en el sistema
class Team(models.Model):
    """ equipo  """
    name = models.CharField(max_length=50)
    shield = models.ImageField(upload_to='shields/')
    team = models.ImageField(upload_to='teams/')
    pub_date = models.DateField(auto_now_add=True)

#sirve para que cuando se llame a un nombre del quipo se indique, que se cooncatene
    def __str__(self):
        return self.name

#Hereda de model
#team equipo al cual pertenece y tiene una llave foranea apuntando a la tabla Team, el caso de borrar se pueden utilizar varios metodos

class Player(models.Model):
    """ ciclista """
    team = models.ForeignKey('Team', on_delete=models.PROTECT,related_name='get_players' )
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    photo = models.ImageField(upload_to='players/')
    pub_date = models.DateField(auto_now_add=True)
    height = models.DecimalField(max_digits=3, decimal_places=2)
    weight = models.IntegerField()
    comment = models.CharField(max_length=200, blank=True)
    
    def __str__(self):
        return self.first_name + " " + self.last_name